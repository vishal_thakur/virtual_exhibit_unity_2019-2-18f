﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GetAllAdvertsRoot
{
    public int result_code;
    public List<GetAllAdvertsResultData> result_data;
}
