﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AdvertInfoDataPost
{
    public float position_x;
    public float position_y;
    public float position_z;

    public List<AdvertSliderImage> slider_images = new List<AdvertSliderImage>();
}
