﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SaveAdvertsRootObject
{
    public List<AdvertInfoDataPost> input_data = new List<AdvertInfoDataPost>();
}
