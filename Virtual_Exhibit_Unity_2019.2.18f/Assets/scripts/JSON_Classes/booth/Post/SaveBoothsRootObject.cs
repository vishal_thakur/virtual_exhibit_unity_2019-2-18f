﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SaveBoothsRootObject
{
    public List<BoothInfoDataPost> input_data = new List<BoothInfoDataPost>();
}
