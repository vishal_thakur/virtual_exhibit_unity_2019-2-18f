﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BoothInfoDataPost
{
    public float position_x;
    public float position_y;
    public float position_z;

    public string logo_image_url;
    public string pdf_url;
    public string video_call_url;
    public string video_url;

    public List<BoothSliderImage> slider_images;
}
