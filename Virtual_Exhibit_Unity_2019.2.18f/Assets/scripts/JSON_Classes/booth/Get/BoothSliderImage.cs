﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BoothSliderImage
{
    public string slider_image_url;
}
