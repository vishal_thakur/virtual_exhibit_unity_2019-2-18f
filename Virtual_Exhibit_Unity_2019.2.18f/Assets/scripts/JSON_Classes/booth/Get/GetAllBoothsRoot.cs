﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GetAllBoothsRoot
{
    public int result_code;
    public List<GetAllBoothsResultData> result_data;
}
