﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class AppManager : MonoBehaviour
{
    //Canvas for PC and Mobile
    [SerializeField]
    List<GameObject>  PcParameters, MobileParameters;

    //Represents the Unit's specific to Entrance Scene reception Area and the Entrance Room itself
    [SerializeField]
    GameObject EntranceRoomParentRef;

    //Represents the Unit's specific to Exhibit like booths , Chairs etc
    [SerializeField]
    GameObject ExhibitRef;


    //Screen Used to show Fade to/From black
    [Space(10)]
    [SerializeField]
    Image FadeInOutScreenRef;


    //Booth manager and Advert manager References
    [SerializeField]
    BoothManager boothManagerRef;

    //Represents All the Booth Units in the Scene
    [Space(10)]
    public List<BoothUnit> BoothUnits = new List<BoothUnit>();

    [SerializeField]
    int BoothsLoadedSuccessfully = 0;


    //Advert Manager Ref
    [SerializeField]
    AdvertManager advertManagerRef;

    //Represents All the Advert Units in the Scene
    public List<AdvertUnit> AllAdvertUnits = new List<AdvertUnit>();


    [SerializeField]
    int AdvertsLoadedSuccessfully = 0;

    //Ref of the Player Controller
    [SerializeField]
    public Transform PlayerTransform;





    #region Unity Methods
    //private void OnGUI()
    //{
    //    GUILayout.TextField("platform = " + Globals.CurrentPlatform);
    //}

    void Awake(){
        //Grab the Platform Type
        Globals.CurrentPlatform = isMobile() ? Globals.Platform.mobile : Globals.Platform.pc;

        //Load PLatform Specific Functionality
        if (Globals.CurrentPlatform == Globals.Platform.mobile)
            LoadMobilePlatformSpecficParameters();
        else
            LoadPcPlatformSpecificParameters();
    }


    void Start()
    {
        //Subscribe to Desired Event
        Messenger.AddListener(Events.OnAllBoothsLoaded, OnBoothsInfoLoadedSusccessfully);
        Messenger.AddListener(Events.OnBoothDataLoaded, OnBoothDataLoaded);
        Messenger.AddListener<Image>(Events.OnEnterExhibitionButonClicked, OnEnterExhibitionButonClicked);

    }

    void OnDestroy()
    {
        //UnSubscribe from Subscribed Events
        Messenger.RemoveListener(Events.OnAllBoothsLoaded, OnBoothsInfoLoadedSusccessfully);
        Messenger.RemoveListener(Events.OnBoothDataLoaded, OnBoothDataLoaded);
        Messenger.RemoveListener<Image>(Events.OnEnterExhibitionButonClicked, OnEnterExhibitionButonClicked);
    }

    #endregion


    #region Core


    [DllImport("__Internal")]
    private static extern bool IsMobile();

    public bool isMobile()
    {
#if !UNITY_EDITOR && UNITY_WEBGL
             return IsMobile();
#endif
        return false;
    }

    [SerializeField]
    bool rename = false;

    //private void OnDrawGizmos()
    //{
    //    if (rename) {
    //        RenameBooths();
    //        rename = false;
    //    }

    //}

    //void RenameBooths()
    //{

    //    int BoothIndex = 1;
    //    string boothName = "";

    //    foreach (BoothUnit booth in BoothUnits)
    //    {
    //        boothName = booth.gameObject.name;

    //        booth.gameObject.name = BoothIndex + "_" + boothName;

    //        BoothIndex++;
    //    }
    //}

    public void TeleportToBooth(int index)
    {
        //Decrement index to avoid overflow, The index in map starts with 1 not 0
        index--;

        //Calculate the teleport position as per the selected booth
        Transform teleportPosition = BoothUnits[index].GetComponentInChildren<TeleportPosition>().transform;

        //Repositio player to the Booth
        PlayerTransform.position = new Vector3(teleportPosition.position.x, PlayerTransform.position.y, teleportPosition.position.z);

    }



    //On Exhibition Enter Behaviour
    void EnterExhibitionBehaviour()
    {
        //Show Fade to Black
        StartCoroutine(FadeToBlack());

        Messenger.Broadcast<string>(Events.OnLoadingProgressUpdate, "Loading Booths...");

        //PlayerTransform.GetComponent<RigidbodyFirstPersonController>().cam.enabled = false;

        //Enable Booth Manager
        boothManagerRef.gameObject.SetActive(true);
    }

    //Adjust and Alter Params as needed for Mobi
    void LoadMobilePlatformSpecficParameters()
    {
        //Show Mobile Canvas and hide PC Canvas
        foreach (GameObject pcParameter in PcParameters)
            pcParameter.SetActive(false);

        foreach (GameObject pcParameter in MobileParameters)
            pcParameter.SetActive(true);

        //Grab the player transform for Mobile  Platform
        PlayerTransform = MobileParameters[1].transform;

        PlayerTransform.gameObject.SetActive(true);

        //Grab Player's RigidbodyFirstPersonController Ref
        //RigidbodyFirstPersonController PlayerRigidbodyFirstPersonControllerRef = PlayerTransform.GetComponent<RigidbodyFirstPersonController>();

        //Grab Player's DualJoystickPlayerController ref
        // DualJoystickPlayerController PlayerDualJoystickPlayerControllerRef = PlayerTransform.GetComponent<DualJoystickPlayerController>();

        //PlayerDualJoystickPlayerControllerRef.enabled = true;
        //PlayerRigidbodyFirstPersonControllerRef.enabled = false;

        //Set Player cam Sensitivity as per Mobile platform
        //PlayerRigidbodyFirstPersonControllerRef.mouseLook.XSensitivity = PlayerRigidbodyFirstPersonControllerRef.mouseLook.YSensitivity = Globals.MobileCamSensitivity;



    }


    void LoadPcPlatformSpecificParameters()
    {
        //Show PC Canvas and hide Mobile Canvas
        foreach (GameObject pcParameter in PcParameters)
            pcParameter.SetActive(true);

        foreach (GameObject pcParameter in MobileParameters)
            pcParameter.SetActive(false);

        //Grab the player transform for Mobile platform
        PlayerTransform = PcParameters[1].transform;

        PlayerTransform.gameObject.SetActive(true);

        //Grab Player's RigidbodyFirstPersonController Ref
        //RigidbodyFirstPersonController PlayerRigidbodyFirstPersonControllerRef = PlayerTransform.GetComponent<RigidbodyFirstPersonController>();

        //Grab Player's DualJoystickPlayerController ref
       // DualJoystickPlayerController PlayerDualJoystickPlayerControllerRef = PlayerTransform.GetComponent<DualJoystickPlayerController>();

        //PlayerDualJoystickPlayerControllerRef.enabled = false;
        //PlayerRigidbodyFirstPersonControllerRef.enabled = true;

        //Set Player cam Sensitivity as per PC platform
       // PlayerTransform.GetComponent<RigidbodyFirstPersonController>().mouseLook.XSensitivity = PlayerTransform.GetComponent<RigidbodyFirstPersonController>().mouseLook.YSensitivity = Globals.PcCamSensitivity;

    }

    #endregion

    #region Coroutines

    public IEnumerator InitializeBooths(GetAllBoothsRoot boothsInfo)
    {
        //Save the total Number of booths
        Globals.TotalBooths = boothsInfo.result_data.Count;

        //Iterate through All Booths
        for (int i = 0; i < BoothUnits.Count; i++)
        {
            yield return new WaitForEndOfFrame();
            BoothUnits[i].GetComponent<BoothUnit>().Initialize(boothsInfo.result_data[i]);
        }
    }

    public IEnumerator InitializeAdverts(GetAllAdvertsRoot advertsInfo)
    {
        //Save the total Number of Adverts
        Globals.TotalAdverts = advertsInfo.result_data.Count;

        //Iterate through All Adverts
        for (int i = 0; i < AllAdvertUnits.Count; i++)
        {
            yield return new WaitForEndOfFrame();
            AllAdvertUnits[i].GetComponent<AdvertUnit>().Initialize(advertsInfo.result_data[i]);
        }
    }

    //Fade to Black Screen
    IEnumerator FadeToBlack()
    {
        float imageAlpha = 0;

        //FadeToBlack
        while (FadeInOutScreenRef.color.a < 1)
        {
            FadeInOutScreenRef.color = new Color(0, 0, 0, imageAlpha += Time.deltaTime * Globals.fadeSpeed);

            //Debug.LogError(FadeInOutScreenRef.color.a.ToString());

            yield return new WaitForEndOfFrame();
        }
        StopCoroutine(FadeToBlack());
    }

    //Fade to Normal from Black Screen
    IEnumerator FadeFromBlack()
    {
        float imageAlpha = 1;

        //Fade To Normal from Black
        while (FadeInOutScreenRef.color.a > 0)
        {
            FadeInOutScreenRef.color = new Color(0, 0, 0, imageAlpha -= Time.deltaTime * Globals.fadeSpeed);

            //Debug.LogError(FadeInOutScreenRef.color.a.ToString());

            yield return new WaitForEndOfFrame();
        }
        StopCoroutine(FadeFromBlack());
    }

    #endregion





    #region Callbacks

    void OnBoothDataLoaded()
    {
        //Increment Total Booth Loaded Count
        BoothsLoadedSuccessfully++;

        //Boradcast Loading Progress of Booths
        Messenger.Broadcast<string>(Events.OnLoadingProgressUpdate, "Loading Booths..." + BoothsLoadedSuccessfully + "/" + Globals.TotalBooths);

        //Debug.LogError("Total Booths = " + Globals.TotalBooths);
        //Debug.LogError("Booth load Count = " + BoothsLoadedSuccessfully);

        //If All Booths Have been loaded 
        if (BoothsLoadedSuccessfully >= Globals.TotalBooths)
        {

            //PlayerTransform.GetComponent<RigidbodyFirstPersonController>().cam.enabled = true;

            //Fade out from Black Screen
            Messenger.Broadcast<string>(Events.OnLoadingProgressUpdate, null);

            StartCoroutine(FadeFromBlack());

            //Debug.LogError("Loading Adverts Now!!");
            //Since BoothsInfo has been loaded successfully, now Load adverts Info
            advertManagerRef.Initialize();
        }
    }


    void OnBoothsInfoLoadedSusccessfully()
    {
        //Fade out To Normal Screen

        //Disable Entrance Room
        EntranceRoomParentRef.SetActive(false);

        //Disable player Controller Component
        //PlayerTransform.GetComponent<RigidbodyFirstPersonController>().enabled = false;

        //Wait for X seconds
        // yield return new WaitForSeconds(2) ;


        //Reposition the player so it faces towards The Booths
        PlayerTransform.position = new Vector3(-139.058f, 1.7f, 84.127f);
        PlayerTransform.rotation = Quaternion.Euler(0, 89.315f, 0);

        //PlayerTransform.GetComponent<RigidbodyFirstPersonController>().InitMouseLook();

        //Re-enable the PlayerController Component
        //PlayerTransform.GetComponent<RigidbodyFirstPersonController>().enabled = true;

        //Enable Booth Units
        ExhibitRef.SetActive(true);
    }

    void OnEnterExhibitionButonClicked(Image fadeImage)
    {
        //Save the FadeImage Ref
        FadeInOutScreenRef = fadeImage;

        //Do Exhibition Enter Behaviour
        EnterExhibitionBehaviour();
    }
    #endregion

}
