﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class HUDManager : MonoBehaviour
{

    //Image used to show the Fade to/From Black screen effect
    [SerializeField]
    Image FadeImage;

    [SerializeField]
    Sprite defaultSprite;

    [SerializeField]
    Text LoadingProgressText;

    BoothManager boothManagerRef;


    [SerializeField]
    List<Image> AllImages;

    [SerializeField]
    int activeIndex = 0;

    [SerializeField]
    GameObject boothInfoPanelRef;

    [SerializeField]
    GetAllBoothsResultData selectedBooth;

    void Start()
    {
        //Subscribe To desired events
        Messenger.AddListener<GetAllBoothsResultData>(Events.OnBoothInteracted, OnBoothInteracted);
        Messenger.AddListener<string>(Events.OnLoadingProgressUpdate , OnLoadingProgressUpdate);

        //Save Ref of Default sprite to reset When Active booth Is Changed
        defaultSprite = AllImages[0].sprite;
    }


    private void OnDestroy()
    {
        //UnSubscribe from subscribed events
        Messenger.RemoveListener<GetAllBoothsResultData>(Events.OnBoothInteracted, OnBoothInteracted);
        Messenger.RemoveListener<string>(Events.OnLoadingProgressUpdate, OnLoadingProgressUpdate);
    }



    //Load Carousel's Next Image
    public void Next()
    {
        //increment index
        activeIndex++;

        Refresh();
    }

    //Load Carousel's Previous Image
    public void Previous()
    {
        //decrement index
        activeIndex--;

        Refresh();
    }


    //Show the Carousel's Image at active index
    void Refresh()
    {
        //Max Overflow Prevention
        if (activeIndex >= AllImages.Count)
            activeIndex = 0;

        //Min Overflow Prevention
        if (activeIndex < 0)
            activeIndex = AllImages.Count - 1;


        for (int i = 0; i < AllImages.Count; i++)
        {
            if (i == activeIndex)
            {
                AllImages[i].gameObject.SetActive(true);
            }
            else
                AllImages[i].gameObject.SetActive(false);
        }

    }


    public void LoadPDF()
    {
        if(Globals.CurrentPlatform == Globals.Platform.pc)
            Application.ExternalEval("window.open(\"" + selectedBooth.pdf_url + "\")");
        else
          Application.OpenURL(selectedBooth.pdf_url);
    }

    public void LoadYoutubeVideo()
    {
        if (Globals.CurrentPlatform == Globals.Platform.pc)
            Application.ExternalEval("window.open(\"" + selectedBooth.video_url + "\")");
        else
            Application.OpenURL(selectedBooth.video_url);
    }

    public void LoadVideoCall()
    {
        //TODO VISHAL Implement Start of Video call here
    }



    #region Callbacks
    void OnBoothInteracted(GetAllBoothsResultData boothData)
    {
        //Load Booth data into the Respective Places
        //Setup PDF URL as per boothData
        //Setup VideoCallURL as per boothData
        //Setup VideoURL as per boothData
        //Setup Slider Images as per the Booth data


        //If the interacted booth is not same as the last interacted booth
        if (selectedBooth.booth_id != boothData.booth_id)
        {
            //reset All Images
            foreach (Image img in AllImages)
            {
                img.sprite = defaultSprite;
            }

            selectedBooth = boothData;

            //Load Booth's Slider Images
            StartCoroutine(LoadAllSliderImages(selectedBooth.slider_images));
        }
        else
        {
        }

        //Show Booth info panel
        boothInfoPanelRef.gameObject.SetActive(true);

    }

    public void OnStartButtonClicked()
    {
        //Broadcast EnterExhibition Button has been Clicked
        Messenger.Broadcast<Image>(Events.OnEnterExhibitionButonClicked , FadeImage);
    }

    void OnLoadingProgressUpdate(string message)
    {
        //If Empty/Null Message was broadcasted then Just Disable the ProgressText
        if (string.IsNullOrEmpty(message))
        {
            LoadingProgressText.gameObject.SetActive(false);
            return;
        }
        else
        {
            //Enable LoadinProgress Text OBJ
            LoadingProgressText.gameObject.SetActive(true);
            //Show loading progress update
            LoadingProgressText.text = message;
        }

    }


    //Iterates through all Logo meshes and Applies the Image
    IEnumerator LoadAllSliderImages(List<BoothSliderImage> sliderImageURLs)
    {
        //Download All Slider nImage from the Given URLs of the Advert's Slider Images
        for (int i = 0; i < sliderImageURLs.Count; i++)
        {
            yield return new WaitForEndOfFrame();

            //Download Booth Slider URL
            UnityWebRequest wwwSliderImage = UnityWebRequestTexture.GetTexture(sliderImageURLs[i].slider_image_url);

            yield return wwwSliderImage.SendWebRequest();

            //Error
            if (wwwSliderImage.isNetworkError || wwwSliderImage.isHttpError)
            {
                Debug.LogError(wwwSliderImage.error);
            }
            else//Success
            {
                //make texture
                Texture textureSliderImage = ((DownloadHandlerTexture)wwwSliderImage.downloadHandler).texture;

                Texture2D texture2D = new Texture2D(textureSliderImage.width, textureSliderImage.height);
                texture2D.LoadImage(wwwSliderImage.downloadHandler.data);

                Rect rec = new Rect(0, 0, texture2D.width, texture2D.height);
                Sprite spriteToUse = Sprite.Create(texture2D, rec, new Vector2(0.5f, 0.5f), 100);
                AllImages[i].sprite = spriteToUse;
                Debug.LogError("Done loading");
            }

            wwwSliderImage.Dispose();
        }
    }
    #endregion
}
