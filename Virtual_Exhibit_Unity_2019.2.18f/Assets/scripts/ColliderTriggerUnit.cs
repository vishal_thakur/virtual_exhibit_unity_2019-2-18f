﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class ColliderTriggerUnit : MonoBehaviour
{
    public UnityEvent OnTriggerEnterMethods;
    public UnityEvent OnTriggerExitMethods;


    private void OnTriggerEnter(Collider other)
    {
        OnTriggerEnterMethods.Invoke();
    }

    private void OnTriggerExit(Collider other)
    {
        OnTriggerExitMethods.Invoke();
    }

}
