﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class AdvertUnit : MonoBehaviour
{

    //represents the Wall logo images and the Interactive panel's Logo Image
    [SerializeField]
    MeshRenderer sliderImageMesh;


    [SerializeField]
    List<Texture> AllSliderImageTextures = new List<Texture>();

    //Represents the whole Info for this Advert
    [Space(10)]
    [SerializeField]
    GetAllAdvertsResultData myData;

    //[SerializeField]
    //string redirectURL = "https://nationalasianweddingshow.co.uk/home/";


    
    public void Initialize(GetAllAdvertsResultData boothData)
    {
        //Save Advert Data
        myData = boothData;

        StartCoroutine(DownloadAndApplyAdvertImages(myData.slider_images));
    }


    //Iterates through all Logo meshes and Applies the Image
    IEnumerator DownloadAndApplyAdvertImages(List<AdvertSliderImage> sliderImageURLs)
    {
        //Download All Slider nImage from the Given URLs of the Advert's Slider Images

        foreach(AdvertSliderImage sliderImage in sliderImageURLs)
        {
            yield return null;

            //Download Advert Slider URL
            UnityWebRequest wwwFrontAd = UnityWebRequestTexture.GetTexture(sliderImage.slider_image_url);
            yield return wwwFrontAd.SendWebRequest();
            Texture textureSliderImage = null;

            //Error
            if (wwwFrontAd.isNetworkError || wwwFrontAd.isHttpError)
            {
                Debug.Log(wwwFrontAd.error);
            }
            else//Success
            {
                //make texture
                textureSliderImage = ((DownloadHandlerTexture)wwwFrontAd.downloadHandler).texture;

                //save Texture in the All SliderImageTextures Array
                AllSliderImageTextures.Add(textureSliderImage);

            }

            wwwFrontAd.Dispose();
        }

        //If multiple Images are available then play the Slideshow
        if (AllSliderImageTextures.Count > 1)
        {
            //Now we have the Images now start the SlideShow
            StartCoroutine(DoSlideShow());
        }
        else if(AllSliderImageTextures.Count == 1)//Apply the Single Image
        {
            sliderImageMesh.sharedMaterial.SetTexture("_MainTex", AllSliderImageTextures[0]);
        }
        else
        {
            //Do Nothing
        }
    }


    IEnumerator DoSlideShow()
    {
        while (true)
        {
            yield return null;
            foreach (Texture slideImageTexture in AllSliderImageTextures)
            {
                if (slideImageTexture != null)
                {
                    //Apply It to The advert MeshRenderer
                    sliderImageMesh.material.SetTexture("_MainTex", slideImageTexture);
                    yield return new WaitForSeconds(Globals.AdvertSliderImageDuration);

                }
                else
                    yield return null;
            }
        }
    }

    //public void Interact()
    //{
    //    //Here comes the On advet Interacted Functionality
    //    //Messenger.Broadcast <GetAllBoothsResultData>(Events.OnBoothInteracted , myData);
    //}
}
