﻿using System;

public class Events{

    #region Misc
    //Signal What is being Loaded , For now it is shown over the Black fade screen
    public static string OnLoadingProgressUpdate = "OnLoadingProgressUpdate"; 
    #endregion





    #region REST API
    public static string OnRESTAPICallback = "OnRESTAPICallback";
    #endregion


    #region Booths
    //Upon clicking Booth's Interact button
    public static string OnBoothInteracted = "OnBoothInteracted";

    //When all booths info is loaded
    public static string OnAllBoothsLoaded = "OnAllBoothsLoaded";

    //When A booth Unit loads it's Data Successfully
    public static string OnBoothDataLoaded = "OnBoothDataLoaded";
    #endregion



    #region Editor App
    public static string OnEditorBoothEditSelected = "OnEditorBoothEditSelected";
    public static string OnEditorAdvertEditSelected = "OnEditorAdvertEditSelected";
    #endregion


    #region HUD
    //Upon Clicking the Enter Exhibition button
    public static string OnEnterExhibitionButonClicked = "OnEnterExhibitionButonClicked";
    #endregion
}