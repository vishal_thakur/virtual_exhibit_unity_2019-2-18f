﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoEditor_AdvertManager : MonoBehaviour
{
    #region Parameters
    //All Adverts info
    [SerializeField]
    GetAllAdvertsRoot GetAllAdvertsRootObject;
    

    [SerializeField]
    [Space(20)]
    Transform AdvertUnitsHolderParent;

    [SerializeField]
    InfoEditor_AdvertUnit InfoEditor_AdvertUnitPrefab;


    [SerializeField]
    GameObject LoadingPanelObj;



    
    [Header("Form Parameters")]
    [Space(20)]
    [SerializeField] InputField position_x_InputRef;
    [SerializeField] InputField position_y_InputRef;
    [SerializeField] InputField position_z_InputRef;

    [SerializeField] List<InputField> slider_image_InputRefList = new List<InputField>();

    [SerializeField]
    Text FormHeaderText;

    //Represents the Selected Advert Info being Edited
    [SerializeField]
    GetAllAdvertsResultData SelectedAdvertInfo;
    #endregion





    void Start()
    {
        //Fetch all Info
        GetAllAdvertsInfo();
        

        SubscribeToEventBroadcasts();
    }

    void SubscribeToEventBroadcasts()
    {
        //Event Fired When A Unit's Edit is Selected
        Messenger.AddListener<GetAllAdvertsResultData>(Events.OnEditorAdvertEditSelected, OnEditorAdvertEditSelected);
    }





    //Calls Get all booths Info API
    void GetAllAdvertsInfo()
    {
        //Enable Loading Screen
        LoadingPanelObj.SetActive(true);

        //Subscribe To Callback Event
        Messenger.AddListener<string>(Events.OnRESTAPICallback, OnGetAllAdvertsCallback);

        //Call the API
        RestAPICallManager.CallRESTAPI(BestHTTP.HTTPMethods.Get, Globals.GetAllAdvertsRESTAPIURL);
    }



    //Sends all the Adverts Info the the backend
    public void SaveAllAdvertsInfo()
    {
        //Enable Loading Screen
        LoadingPanelObj.SetActive(true);

        //New SaveAdvertsRootObject Obj
        SaveAdvertsRootObject saveAdvertsRootObject = new SaveAdvertsRootObject();

        try
        {

            //FILL IN Data
            foreach (GetAllAdvertsResultData advertData in GetAllAdvertsRootObject.result_data)
            {
                //Create new AdvertInfoDataObj
                AdvertInfoDataPost newAdvertInfoDataPost = new AdvertInfoDataPost();

                //Fill in XYZ pos
                newAdvertInfoDataPost.position_x = advertData.position_x;
                newAdvertInfoDataPost.position_y = advertData.position_y;
                newAdvertInfoDataPost.position_z = advertData.position_z;

                //Slider Images
                newAdvertInfoDataPost.slider_images = new List<AdvertSliderImage>();

                foreach (AdvertSliderImage slider_ImageUrl in advertData.slider_images)
                {
                    AdvertSliderImage newSliderImage = new AdvertSliderImage();
                    newSliderImage.slider_image_url = slider_ImageUrl.slider_image_url;

                    newAdvertInfoDataPost.slider_images.Add(newSliderImage);
                }

                saveAdvertsRootObject.input_data.Add(newAdvertInfoDataPost);
            }
        }
        catch(System.Exception e)
        {
            Debug.LogError(e);
        }

        //Convert TO JSON
        string rawData = JsonUtility.ToJson(saveAdvertsRootObject);

        //Subscribe To OnAddUserToVendor Response Event
        Messenger.AddListener<string>(Events.OnRESTAPICallback, OnSaveAllAdvertsInfoCallback);

        //Call the API
        RestAPICallManager.CallRESTAPI(BestHTTP.HTTPMethods.Post, Globals.SaveAllAdvertsRESTAPIURL, rawData);
    }


    public void RefreshInfo()
    {
        //Delete Previous Advers Units
        foreach(InfoEditor_AdvertUnit child in AdvertUnitsHolderParent.GetComponentsInChildren<InfoEditor_AdvertUnit>())
        {
            Destroy(child.gameObject);
        }
        //Get all adverts Again 
        GetAllAdvertsInfo();
    }



    public void UpdateAndSaveSelectedAdvertInfo()
    {

        //Debug.LogError("Initiating");
        //Nothing to Update
        if (string.IsNullOrEmpty(SelectedAdvertInfo.position_x.ToString()))
            return;

        //Show Loading Panel
        LoadingPanelObj.SetActive(true);

        for (int i = 0; i < GetAllAdvertsRootObject.result_data.Count; i++)
        {
            //If found the Required AdvertInfo
            if(SelectedAdvertInfo.advert_id == GetAllAdvertsRootObject.result_data[i].advert_id)
            {
                //Fill in XYZ positions
                GetAllAdvertsRootObject.result_data[i].position_x = float.Parse(position_x_InputRef.text);
                GetAllAdvertsRootObject.result_data[i].position_y = float.Parse(position_y_InputRef.text);
                GetAllAdvertsRootObject.result_data[i].position_z = float.Parse(position_z_InputRef.text);
               

                //Fill in  Slider Images
                for(int j=0;j< slider_image_InputRefList.Count; j++)
                {
                    if(j < GetAllAdvertsRootObject.result_data[i].slider_images.Count)
                      GetAllAdvertsRootObject.result_data[i].slider_images[j].slider_image_url = slider_image_InputRefList[j].text;
                    else
                    {
                        AdvertSliderImage newAdvertSliderImage = new AdvertSliderImage();
                        newAdvertSliderImage.slider_image_url = slider_image_InputRefList[j].text;
                        GetAllAdvertsRootObject.result_data[i].slider_images.Add(newAdvertSliderImage);
                        //Debug.LogError("Added new Slider Image");
                    }
                }
            }
        }

        //Hide Loading Panel
        LoadingPanelObj.SetActive(false);

      //  Debug.LogError("Updated");
    }








    #region Callbacks
    void OnEditorAdvertEditSelected(GetAllAdvertsResultData advertInfo)
    {
        //Set Header Text
        FormHeaderText.text = "Selected Advert = adver_" + advertInfo.advert_id;

        //Update the selected Advert Info
        SelectedAdvertInfo = advertInfo;

        position_x_InputRef.text = advertInfo.position_x.ToString();
        position_y_InputRef.text = advertInfo.position_y.ToString();
        position_z_InputRef.text = advertInfo.position_z.ToString();
        
        for(int i=0;i< slider_image_InputRefList.Count;i++)
        {
            try
            {
                slider_image_InputRefList[i].text = advertInfo.slider_images[i].slider_image_url;
            }
            catch(System.Exception e)
            {
                slider_image_InputRefList[i].text = "-";
            }
        }
    }



    void OnSaveAllAdvertsInfoCallback(string response)
    {
        Debug.LogError("Got Response = " + response);
        
        //Enable Loading Screen
        LoadingPanelObj.SetActive(false);

        //UnSubscribe To Rest API Response Event
        Messenger.RemoveListener<string>(Events.OnRESTAPICallback, OnSaveAllAdvertsInfoCallback);
    }

    void OnGetAllAdvertsCallback(string response)
    {
        //Debug.LogError("Got Response = " + response);


        ////UpdateUserProfileResponse obj
        //GetAllBoothsRoot GetAllBoothsRootObject = new GetAllBoothsRoot();

        //Deserialize The JSON Object
        try
        {
            //----Success----
            GetAllAdvertsRootObject = JsonUtility.FromJson<GetAllAdvertsRoot>(response);
            
            
            //Load Booth Units as per response
            for(int i=0;i< GetAllAdvertsRootObject.result_data.Count; i++)
            {
                //Spawn a new Unit
                InfoEditor_AdvertUnit newBoothUnit = Instantiate(InfoEditor_AdvertUnitPrefab , AdvertUnitsHolderParent , false);

                //Initialize the new Booth UNIT
                newBoothUnit.Initialize(GetAllAdvertsRootObject.result_data[i]);
            }


        }
        catch (System.Exception e)
        {
            //----Fail----
            Debug.LogError("Something Wrong here");
            return;
        }
        
        //Enable Loading Screen
        LoadingPanelObj.SetActive(false);

        //UnSubscribe To Rest API Response Event
        Messenger.RemoveListener<string>(Events.OnRESTAPICallback, OnGetAllAdvertsCallback);
    }
    #endregion
}
