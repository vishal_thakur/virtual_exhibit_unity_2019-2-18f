﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoEditor_AdvertUnit : MonoBehaviour
{

    [SerializeField]
    Text AdvertNameTextRef;

    [SerializeField]
    GetAllAdvertsResultData AdvertInfo;


    public void Initialize(GetAllAdvertsResultData info)
    {
        //save Info
        AdvertInfo = info;

        AdvertNameTextRef.text = "Advert_" + AdvertInfo.advert_id;
    }
    

    public void Edit()
    {
        //Broadcast this unit selected for editing
        Messenger.Broadcast<GetAllAdvertsResultData>(Events.OnEditorAdvertEditSelected , AdvertInfo);
    }
}
