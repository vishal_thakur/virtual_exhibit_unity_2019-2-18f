﻿using System.Collections;
using System.Collections.Generic;

public static class Globals
{
    #region REST API URLS
    public static string GetAllBoothsRESTAPIURL = "https://www.tnawsvirtual.com/apis/api.php?rquest=getBooths";
    public static string GetAllAdvertsRESTAPIURL = "https://www.tnawsvirtual.com/apis/api.php?rquest=getAdvertisements";


    public static string SaveAllBoothsRESTAPIURL = "https://www.tnawsvirtual.com/apis/api.php?rquest=createBooth";
    public static string SaveAllAdvertsRESTAPIURL = "https://www.tnawsvirtual.com/apis/api.php?rquest=createAdvertisement";
    #endregion


    
    //Represents the timeGap in seconds between each Advert's SliderImages Slideshow
    public static float AdvertSliderImageDuration = 2;


    public static string LoadingProgress = "Loading..";

    //#region PlayerCam
    //public static float MobileCamSensitivity = 1;
    //public static float PcCamSensitivity = 10;
    //#endregion



    #region Misc
    public static float fadeSpeed = 2f;

    public static int TotalBooths = 0;
    public static int TotalAdverts = 0;
    #endregion


    public static Platform CurrentPlatform = Platform.none;

    public enum Platform
    {
        none = 0,
        pc,
        mobile
    }
}
