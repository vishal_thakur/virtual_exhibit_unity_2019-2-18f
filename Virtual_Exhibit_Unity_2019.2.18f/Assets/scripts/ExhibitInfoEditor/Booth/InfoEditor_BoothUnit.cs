﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoEditor_BoothUnit : MonoBehaviour
{

    [SerializeField]
    Text boothNameTextRef;

    [SerializeField]
    GetAllBoothsResultData BoothInfo;


    public void Initialize(GetAllBoothsResultData info)
    {
      
        boothNameTextRef.text = "Booth_" + info.booth_id;

        //save GetAllBoothsResultData Info
        BoothInfo = info;
    }
    

    public void Edit()
    {
        //Broadcast this Booth unit selected for editing
        Messenger.Broadcast<GetAllBoothsResultData>(Events.OnEditorBoothEditSelected , BoothInfo);
    }
}
