﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoEditor_BoothManager : MonoBehaviour
{
    #region Parameters
    //All Booths info
    [SerializeField]
    GetAllBoothsRoot GetAllBoothsRootObject;
    

    [SerializeField]
    [Space(20)]
    Transform BoothUnitsHolderParent;

    [SerializeField]
    InfoEditor_BoothUnit InfoEditor_BoothUnitPrefab;


    [SerializeField]
    GameObject LoadingPanelObj;



    
    [Header("Form Parameters")]
    [Space(20)]
    [SerializeField] InputField position_x_InputRef;
    [SerializeField] InputField position_y_InputRef;
    [SerializeField] InputField position_z_InputRef;

    [SerializeField] InputField logo_image_url_InputRef;
    [SerializeField] InputField pdf_url_InputRef;
    [SerializeField] InputField video_call_url_InputRef;
    [SerializeField] InputField video_url_InputRef;

    [SerializeField] List<InputField> slider_image_InputRefList = new List<InputField>();


    [SerializeField]
    Text FormHeaderText;

    //Represents the Selected Advert Info being Edited
    [SerializeField]
    GetAllBoothsResultData SelectedBoothInfo;
    #endregion



    
    void Start()
    {
        //Fetch all Booths Info
        GetAllBoothsInfo();
        

        SubscribeToEventBroadcasts();
    }

    void SubscribeToEventBroadcasts()
    {
        //Event Fired When A Booth Unit's Edit is Selected
        Messenger.AddListener<GetAllBoothsResultData>(Events.OnEditorBoothEditSelected, OnEditorBoothEditSelected);
    }


    public void QuitApp()
    {
        Application.Quit();
    }



    //Calls Get all booths Info API
    void GetAllBoothsInfo()
    {
        //Enable Loading Screen
        LoadingPanelObj.SetActive(true);


        //Subscribe To Rest API Callback
        Messenger.AddListener<string>(Events.OnRESTAPICallback, OnGetAllBoothsCallback);

        //Get All Booths
        RestAPICallManager.CallRESTAPI(BestHTTP.HTTPMethods.Get, Globals.GetAllBoothsRESTAPIURL);

        //Debug.LogError("Calling API" + Globals.GetAllBoothsRESTAPIURL);
    }
    


    //Sends all the Booths Info the the backend
    public void SaveAllBoothsInfo()
    {
        //Enable Loading Screen
        LoadingPanelObj.SetActive(true);

        //New SaveBoothsRootObject Obj
        SaveBoothsRootObject saveBoothsRootObject = new SaveBoothsRootObject();

        //FILL IN Data
        foreach (GetAllBoothsResultData boothData in GetAllBoothsRootObject.result_data)
        {
            //Create new BoothInfoDataObj
            BoothInfoDataPost newBoothInfoDataPost = new BoothInfoDataPost();

            //Fill in XYZ pos
            newBoothInfoDataPost.position_x = boothData.position_x;
            newBoothInfoDataPost.position_y = boothData.position_y;
            newBoothInfoDataPost.position_z = boothData.position_z;

            newBoothInfoDataPost.logo_image_url = boothData.logo_image_url;
            newBoothInfoDataPost.pdf_url = boothData.pdf_url;
            newBoothInfoDataPost.video_call_url = boothData.video_call_url;
            newBoothInfoDataPost.video_url = boothData.video_url;

            //Slider Images
            newBoothInfoDataPost.slider_images = new List<BoothSliderImage>();

            foreach(BoothSliderImage slider_ImageUrl in boothData.slider_images)
            {
                BoothSliderImage newSliderImage = new BoothSliderImage();
                newSliderImage.slider_image_url = slider_ImageUrl.slider_image_url;

                newBoothInfoDataPost.slider_images.Add(newSliderImage);
            }

            saveBoothsRootObject.input_data.Add(newBoothInfoDataPost);
        }


        //Convert TO JSON
        string rawData = JsonUtility.ToJson(saveBoothsRootObject);
        Debug.LogError(rawData);
        //Subscribe To OnAddUserToVendor Response Event
        Messenger.AddListener<string>(Events.OnRESTAPICallback, OnSaveAllBoothsInfoCallback);

        //Call the API
        RestAPICallManager.CallRESTAPI(BestHTTP.HTTPMethods.Post, Globals.SaveAllBoothsRESTAPIURL, rawData);
    }


    public void RefreshInfo()
    {
        //Delete Previous Booths Units
        foreach(InfoEditor_BoothUnit child in BoothUnitsHolderParent.GetComponentsInChildren<InfoEditor_BoothUnit>())
        {
            Destroy(child.gameObject);
        }
        //Get all Booths Again 
        GetAllBoothsInfo();
    }





    public void UpdateAndSaveSelectedBoothInfo()
    {

        //Debug.LogError("Initiating");
        //Nothing to Update
        if (string.IsNullOrEmpty(SelectedBoothInfo.position_x.ToString()))
            return;

        //Show Loading Panel
        LoadingPanelObj.SetActive(true);

        for (int i = 0; i < GetAllBoothsRootObject.result_data.Count; i++)
        {
            //If found the Required BoothInfo
            if (SelectedBoothInfo.booth_id == GetAllBoothsRootObject.result_data[i].booth_id)
            {
                //Fill in XYZ positions
                GetAllBoothsRootObject.result_data[i].position_x = float.Parse(position_x_InputRef.text);
                GetAllBoothsRootObject.result_data[i].position_y = float.Parse(position_y_InputRef.text);
                GetAllBoothsRootObject.result_data[i].position_z = float.Parse(position_z_InputRef.text);

                //Fill in PDFURL
                GetAllBoothsRootObject.result_data[i].pdf_url = pdf_url_InputRef.text;

                //Fill in Video Call URL
                GetAllBoothsRootObject.result_data[i].video_call_url = video_call_url_InputRef.text;

                //Fill in Video URL
                GetAllBoothsRootObject.result_data[i].video_url = video_url_InputRef.text;

                //Fill in Slider Images
                for (int j = 0; j < slider_image_InputRefList.Count; j++)
                {
                    if (j < GetAllBoothsRootObject.result_data[i].slider_images.Count)
                        GetAllBoothsRootObject.result_data[i].slider_images[j].slider_image_url = slider_image_InputRefList[j].text;
                    else
                    {
                        BoothSliderImage newBoothSliderImage = new BoothSliderImage();
                        newBoothSliderImage.slider_image_url = slider_image_InputRefList[j].text;
                        GetAllBoothsRootObject.result_data[i].slider_images.Add(newBoothSliderImage);
                        //Debug.LogError("Added new Slider Image");
                    }
                }
            }
        }

        //Hide Loading Panel
        LoadingPanelObj.SetActive(false);

        //  Debug.LogError("Updated");
    }




    #region Callbacks
    void OnEditorBoothEditSelected(GetAllBoothsResultData boothInfo)
    {
        //Set Header Text
        FormHeaderText.text = "Selected Booth = Booth_" + boothInfo.booth_id;

        //Update the selected Booth Info
        SelectedBoothInfo = boothInfo;

        position_x_InputRef.text = boothInfo.position_x.ToString();
        position_y_InputRef.text = boothInfo.position_y.ToString();
        position_z_InputRef.text = boothInfo.position_z.ToString();

        logo_image_url_InputRef.text = boothInfo.logo_image_url;
        pdf_url_InputRef.text = boothInfo.pdf_url;
        video_call_url_InputRef.text = boothInfo.video_call_url;
        video_url_InputRef.text = boothInfo.video_url;
        
        for(int i=0;i< slider_image_InputRefList.Count;i++)
        {
            try
            {
                slider_image_InputRefList[i].text = boothInfo.slider_images[i].slider_image_url;
            }
            catch(System.Exception e)
            {
                slider_image_InputRefList[i].text = "-";
            }
        }
    }



    void OnSaveAllBoothsInfoCallback(string response)
    {
        Debug.LogError("OnSaveAllBoothsInfoCallback Response = " + response);
        
        //Enable Loading Screen
        LoadingPanelObj.SetActive(false);

        //UnSubscribe To Rest API Response Event
        Messenger.RemoveListener<string>(Events.OnRESTAPICallback, OnSaveAllBoothsInfoCallback);
    }

    void OnGetAllBoothsCallback(string response)
    {
        //Debug.LogError("Got Response = " + response);
        

        //Deserialize The JSON Object
        try
        {
            //----Success----
            GetAllBoothsRootObject = null;
            GetAllBoothsRootObject = JsonUtility.FromJson<GetAllBoothsRoot>(response);
            
            
            //Load Booth Units as per response
            for(int i=0;i< GetAllBoothsRootObject.result_data.Count; i++)
            {
                //Spawn a new Unit
                InfoEditor_BoothUnit newBoothUnit = Instantiate(InfoEditor_BoothUnitPrefab , BoothUnitsHolderParent , false);

                //Initialize the new Booth UNIT
                newBoothUnit.Initialize(GetAllBoothsRootObject.result_data[i]);
            }


        }
        catch (System.Exception e)
        {
            //----Fail----
            Debug.LogError("Something Wrong here");
            return;
        }
        
        //Enable Loading Screen
        LoadingPanelObj.SetActive(false);

        //UnSubscribe To Rest API Response Event
        Messenger.RemoveListener<string>(Events.OnRESTAPICallback, OnGetAllBoothsCallback);
    }
    #endregion
}
