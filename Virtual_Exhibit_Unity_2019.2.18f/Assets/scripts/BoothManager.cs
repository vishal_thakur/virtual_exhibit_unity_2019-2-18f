﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoothManager : MonoBehaviour
{

    [SerializeField]
    GetAllBoothsRoot GetAllBoothsRootObject = new GetAllBoothsRoot();


    [SerializeField]
    AppManager appManagerRef;

    //If Set to True then Only Info of Current Booths in scene is sent to the Backend
    [SerializeField]
    bool OnlySyncDataToBackend = false;


    [SerializeField]
    SaveBoothsRootObject saveBoothsRootObject;
    


    private void Start()
    {
        Initialize();

    }



    void Initialize()
    {
        if (!OnlySyncDataToBackend)
        {
            //Subscribe To Rest API Callback
            Messenger.AddListener<string>(Events.OnRESTAPICallback, OnGetAllBooths);

            //Get All Booths
            RestAPICallManager.CallRESTAPI(BestHTTP.HTTPMethods.Get, Globals.GetAllBoothsRESTAPIURL);
        }
        else
        {
            //Save Environment Data to Backend
            SaveDataAsPerScene();
        }
    }


    void SaveDataAsPerScene()
    {
        //New SaveBoothsRootObject Obj
        saveBoothsRootObject = new SaveBoothsRootObject();

        //FILL IN Data
        foreach (BoothUnit boothData in appManagerRef.BoothUnits)
        {
            //Create new BoothInfoDataObj
            BoothInfoDataPost newBoothInfoDataPost = new BoothInfoDataPost();

            //Fill in XYZ pos
            newBoothInfoDataPost.position_x = boothData.transform.localPosition.x;
            newBoothInfoDataPost.position_y = boothData.transform.localPosition.y;
            newBoothInfoDataPost.position_z = boothData.transform.localPosition.z;

            //Set a default value for the LogoImageURL which can be changed from the info Editor
            //Set a default value for the pdfURL which can be changed from the info Editor
            //Set a default value for the VideoCallUrl which can be changed from the info Editor
            //Set a default value for the VideoURL which can be changed from the info Editor            
            newBoothInfoDataPost.logo_image_url = "https://www.tnawsvirtual.com/Booth_Info/logoImages/boothImage.png";
            newBoothInfoDataPost.pdf_url = "www.pdfURL.com";
            newBoothInfoDataPost.video_call_url = "www.videoCallURL.com";
            newBoothInfoDataPost.video_url = "www.videoURL.com";


            //Slider Images
            newBoothInfoDataPost.slider_images = new List<BoothSliderImage>();

            //set default values for Slider Images 5 each
            for(int i = 0;i < 5;i++)
            {
                BoothSliderImage newSliderImage = new BoothSliderImage();
                newSliderImage.slider_image_url = "www.SliderURL" + i + ".com";

                newBoothInfoDataPost.slider_images.Add(newSliderImage);
            }

            saveBoothsRootObject.input_data.Add(newBoothInfoDataPost);
        }


        //Convert TO JSON
        string rawData = JsonUtility.ToJson(saveBoothsRootObject);
        Debug.LogError(rawData);
        //Subscribe To OnAddUserToVendor Response Event
        Messenger.AddListener<string>(Events.OnRESTAPICallback, OnSaveAllBoothsInfoCallback);

        //Call the API
        RestAPICallManager.CallRESTAPI(BestHTTP.HTTPMethods.Post, Globals.SaveAllBoothsRESTAPIURL, rawData);

    }


    #region Callbacks
    void OnGetAllBooths(string response)
    {
        //Deserialize The JSON Object
        try
        {
            //----Success----
            GetAllBoothsRootObject = JsonUtility.FromJson<GetAllBoothsRoot>(response);
        }
        catch (System.Exception e)
        {
            //----Fail----
            Debug.LogError("Something Wrong here");
            return;
        }

        //Feed the Data recieved to All the Booth Units
        appManagerRef.StartCoroutine(appManagerRef.InitializeBooths(GetAllBoothsRootObject));


        //UnSubscribe To Rest API Response Event
        Messenger.RemoveListener<string>(Events.OnRESTAPICallback, OnGetAllBooths);

        //Broadcast to ALl that Booths Info has been loaded Successfully
        Messenger.Broadcast(Events.OnAllBoothsLoaded);
    }
    



    void OnSaveAllBoothsInfoCallback(string response)
    {
        Debug.LogError("OnSaveAllBoothsInfoCallback Response = " + response);
        
        //UnSubscribe To Rest API Response Event
        Messenger.RemoveListener<string>(Events.OnRESTAPICallback, OnSaveAllBoothsInfoCallback);
    }

    //void OnBoothInteracted(GetAllBoothsResultData  boothInfo)
    //{
    //    //Save the Selected Booth's info
    //    selectedBooth = boothInfo;
    //}
    #endregion
}
