﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdvertManager : MonoBehaviour
{


    [SerializeField]
    GetAllAdvertsRoot GetAllAdvertsRootObject = new GetAllAdvertsRoot();


    [SerializeField]
    AppManager appManagerRef;

    //If Set to True then Only Info of Current Booths in scene is sent to the Backend
    [SerializeField]
    bool OnlySyncDataToBackend = false;


    [SerializeField]
    SaveAdvertsRootObject saveAdvertsRootObject;


    //private void Start()
    //{
    //    Initlialize();
    //}


    public void Initialize()
    {
        //If advert Manager Is disabled then Enable it
        if(!gameObject.activeSelf)
           gameObject.SetActive(true);

        if (!OnlySyncDataToBackend)
        {
            //Subscribe To Rest API Callback
            Messenger.AddListener<string>(Events.OnRESTAPICallback, OnGetAllAdverts);

            //Get All Booths
            RestAPICallManager.CallRESTAPI(BestHTTP.HTTPMethods.Get, Globals.GetAllAdvertsRESTAPIURL);
        }
        else
        {
            //Save Environment Data to Backend
            StartCoroutine(SaveDataAsPerScene());
        }
    }


    IEnumerator SaveDataAsPerScene()
    {
        //New SaveBoothsRootObject Obj
        saveAdvertsRootObject = new SaveAdvertsRootObject();

        //FILL IN Data
        foreach (AdvertUnit advertUnit in appManagerRef.AllAdvertUnits)
        {
            yield return null;
            //Create new BoothInfoDataObj
            AdvertInfoDataPost newAdvertInfoDataPost = new AdvertInfoDataPost();

            //Fill in XYZ pos
            newAdvertInfoDataPost.position_x = advertUnit.transform.localPosition.x;
            newAdvertInfoDataPost.position_y = advertUnit.transform.localPosition.y;
            newAdvertInfoDataPost.position_z = advertUnit.transform.localPosition.z;
            
            //Slider Images
            newAdvertInfoDataPost.slider_images = new List<AdvertSliderImage>();

            //set default values for Slider Images 5 each
            for (int i = 0; i < 5; i++)
            {
                yield return null;
                AdvertSliderImage newSliderImage = new AdvertSliderImage();
                newSliderImage.slider_image_url = "https://www.tnawsvirtual.com/advert_info/sliderImages/Ad_1.png";

                newAdvertInfoDataPost.slider_images.Add(newSliderImage);
            }

            saveAdvertsRootObject.input_data.Add(newAdvertInfoDataPost);
        }
        

        //Convert TO JSON
        string rawData = JsonUtility.ToJson(saveAdvertsRootObject);
        Debug.LogError(rawData);
        //Subscribe To OnAddUserToVendor Response Event
        Messenger.AddListener<string>(Events.OnRESTAPICallback, OnSaveAllAdvertsInfoCallback);

        //Call the API
        RestAPICallManager.CallRESTAPI(BestHTTP.HTTPMethods.Post, Globals.SaveAllAdvertsRESTAPIURL, rawData);

    }


    #region Callbacks
    void OnGetAllAdverts(string response)
    {
        //Debug.LogError(response);
        //Deserialize The JSON Object
        try
        {
            //----Success----
            GetAllAdvertsRootObject = JsonUtility.FromJson<GetAllAdvertsRoot>(response);
        }
        catch (System.Exception e)
        {
            //----Fail----
            Debug.LogError("Something Wrong here" + e);
            return;
        }

        //Feed the Data recieved to All the Advert Units
        appManagerRef.StartCoroutine(appManagerRef.InitializeAdverts(GetAllAdvertsRootObject));


        //UnSubscribe To Rest API Response Event
        Messenger.RemoveListener<string>(Events.OnRESTAPICallback, OnGetAllAdverts);
    }




    void OnSaveAllAdvertsInfoCallback(string response)
    {
       // Debug.LogError("OnSaveAllAdvertsInfoCallback Response = " + response);

        //UnSubscribe To Rest API Response Event
        Messenger.RemoveListener<string>(Events.OnRESTAPICallback, OnSaveAllAdvertsInfoCallback);
    }
    #endregion
}
