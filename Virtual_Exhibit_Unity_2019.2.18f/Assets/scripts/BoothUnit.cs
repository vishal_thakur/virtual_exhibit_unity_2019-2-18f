﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class BoothUnit : MonoBehaviour
{

    //represents the Wall logo images and the Interactive panel's Logo Image
    [SerializeField]
    List<MeshRenderer> LogoMeshes = new List<MeshRenderer>();

    [SerializeField]
    Texture logoTexture;

    //Represents the whole Info for this booth
    [Space(10)]
    [SerializeField]
    GetAllBoothsResultData myData;

    //[SerializeField]
    string redirectURL = "https://nationalasianweddingshow.co.uk/home/";

    [SerializeField]
    GameObject InteractivePanelCanvas;

    
    public void Initialize(GetAllBoothsResultData boothData)
    {
        //Save Booth Data
        myData = boothData;

        StartCoroutine(DownloadAndApplyBoothLogoImages(myData.logo_image_url));
    }


    //Iterates through all Logo meshes and Applies the Image
    IEnumerator DownloadAndApplyBoothLogoImages(string logoImageURL)
    {
        //Download Advert Slider URL
        UnityWebRequest wwwFrontAd = UnityWebRequestTexture.GetTexture(logoImageURL);
        yield return wwwFrontAd.SendWebRequest();

        //Error
        if (wwwFrontAd.isNetworkError || wwwFrontAd.isHttpError)
        {
            Debug.LogError("Failed to fetch Booth Logo Image for booth " + myData.booth_id +  " And error = " + wwwFrontAd.error);
        }
        else//Success
        {
            Texture textureImage = ((DownloadHandlerTexture)wwwFrontAd.downloadHandler).texture;

            //save Texture in the All SliderImageTextures Array
            logoTexture = textureImage;

            //Apply to all logo mesges
            if (logoTexture != null)
            {
                foreach (MeshRenderer logoMesh in LogoMeshes)
                {
                    yield return new WaitForEndOfFrame();
                    logoMesh.sharedMaterial.SetTexture("_MainTex", logoTexture);
                }
            }
        }
        Messenger.Broadcast(Events.OnBoothDataLoaded);
        wwwFrontAd.Dispose();
    }



    public void Interact()
    {
        //BroadCast the Following Params which are loaded into the Main Booth Canvas to show the info of the Booth interacted with
        //PDF URL
        //Video Call URL
        //Video URL
        //Slider Images URL array
        Messenger.Broadcast <GetAllBoothsResultData>(Events.OnBoothInteracted , myData);
    }

    private void OnTriggerEnter(Collider other)
    {
        //Enable Interactive panel canvas
        InteractivePanelCanvas.SetActive(true);
    }

    private void OnTriggerExit(Collider other)
    {
        //Disable Interactive panel canvas
        InteractivePanelCanvas.SetActive(false);
    }
}
